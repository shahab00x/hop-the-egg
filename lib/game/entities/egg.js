ig.module(
	'game.entities.egg'
)
.requires(
    'plugins.box2d.entity'
)
.defines(function(){

EntityEgg = ig.Box2DEntity.extend({
	debug : 0, // prints debug messages if it is 1
	// The players (collision) size is a bit smaller than the animation
	// frames, so we have to move the collision box a bit (offset)
	size: {x: 25, y:32},
	collides: ig.Entity.COLLIDES.NEVER,
	
	animSheet: new ig.AnimationSheet( 'media/egg.png', 25, 33 ),	
		
    
    //local variables
    entity_state : [],
    t : {},
    speed : 20,
    jump_force : new b2.Vec2(0,-400),
    game_over_time : new ig.Timer(),
    fall_y : 0,
    
    // Swipe properties
    swipe_origin : [],
    swipe_started : [],
    swipes: {
        up_released : "UP_RELEASED",
        up_holding : "UP_HOLDING",
        down_released : "DOWN_RELEASED",
        down_holding : "DOWN_HOLDING",
        left_released : "LEFT_RELEASED",
        left_holding : "LEFT_HOLDING",
        right_released : "RIGHT_RELEASED",
        right_holding : "RIGHT_HOLDING",
    },
    swipe_length : 40,
    
    // game status
    game_over : "no",
    
	init: function( x, y, settings ) {  
        this.t = ig.input.mouse; // touch input
        this.entity_state = ["idle"];
		// Add the animations
		this.addAnim( 'idle', .2, [0]);
        this.addAnim( 'broken', 1, [1]);
        
		this.parent( x, y, settings );
        if ( this.body != null){
            this.fall_y = this.body.GetPosition().y;
            this.body.SetBullet(true);
        }
	},
    
    jump: function(){
        this.body.ApplyImpulse( this.jump_force, this.body.GetPosition());
        
        if (this.debug)
            console.log('jumping');
    },
    
    move_left: function(){ 
        this.body.SetLinearVelocity(new b2.Vec2(-this.speed,this.body.GetLinearVelocity().y));
        
        if (this.debug)
            console.log('left');
    },
    
    move_right: function(){
        this.body.SetLinearVelocity(new b2.Vec2( this.speed,this.body.GetLinearVelocity().y));  
        
        if (this.debug)
            console.log('right');
    },
    
    idle : function(){
        this.body.SetLinearVelocity(new b2.Vec2(0, this.body.GetLinearVelocity().y));
    },
    
    is_touch_jump: function (s){ // t = touch position, e = entity position
        // If t is inside a parabola ( y = x^2 ) above the entity, it is a jump
//        return false;
//        if (  (t[1] - e[1]) < -(Math.pow((t[0] - e[0]),2))/10)
//            return true;
//        else
//            return false; 
        
        if (s == "UP")
            return true;
        else 
            return false;
    },
    
    is_standing: function() {
        // Iterate over all contact edges for this body. m_contactList is 
        // a linked list, not an array, hence the "funny" way of iterating 
        // over it
        for( var edge = this.body.m_contactList; edge; edge = edge.next ) {

            // Get the normal vector for this contact
            var normal = edge.contact.m_manifold.normal;

            // If the normal vector for this contact is pointing upwards
            // (y is less than 0), then this body is "standing" on something
            if( normal.y < 0 ) {
                if (this.debug)
                    console.log( 'standing!' );
                return true;

            }                
        }

        return false;
    },
    is_touching_wall: function() {
        // Iterate over all contact edges for this body. m_contactList is 
        // a linked list, not an array, hence the "funny" way of iterating 
        // over it
        for( var edge = this.body.m_contactList; edge; edge = edge.next ) {

            // Get the normal vector for this contact
            var normal = edge.contact.m_manifold.normal;

            // If the normal vector for this contact is pointing upwards
            // (y is less than 0), then this body is "standing" on something
            if( normal.y == 0 )
            {
//                this.body.ApplyImpulse( new b2.Vec2(-500,0), this.body.GetPosition());
                var p = this.body.GetPosition();
                p.x = p.x + normal.x * .4;
                this.body.pos = p;
                if (this.debug)
                    console.log( 'touching wall!' );
                return true;

            }                
        }

        return false;
    },
    
    swiped_where : function(swipe_origin, swipe_started, touch){
        if (swipe_started != true){
            var m_x = touch.x;
            var m_y = touch.y;
            swipe_origin = {x:m_x, y:m_y};
            return [swipe_origin, true, "NONE"];
        }
        
        if (swipe_started){
            var m_x = touch.x;
            var m_y = touch.y;
            var diff_y = m_y - swipe_origin.y;
            var diff_x = m_x - swipe_origin.x;
                           
//            console.log(touch);
            if (touch.state != 'up') 
                return [swipe_origin, swipe_started, "NONE"];
            var swipe_started = false;
            
            var hold_status = "_HOLDING";
            if (touch.state == 'up')
                hold_status = '_RELEASED'
            if (Math.sqrt( Math.pow(diff_x,2) + Math.pow(diff_y,2)) <this.swipe_length)
                return [swipe_origin, swipe_started, "NONE"];
            
            var rad = Math.atan2(diff_y, diff_x); 
            if ( diff_x == 0 && diff_y > 0)
                return [swipe_origin, swipe_started, "DOWN" + hold_status];
            else if (diff_x == 0 && diff_y < 0 ) 
                return [swipe_origin, swipe_started, "UP" + hold_status];
            else if (rad < Math.PI/4 && rad > -Math.PI/4)
                return [swipe_origin, swipe_started, "RIGHT" + hold_status];
            else if (rad < 3 * Math.PI/4 && rad > Math.PI/4)
                return [swipe_origin, swipe_started, "DOWN" + hold_status];
            else if (rad < -3 * Math.PI/4 || rad > 3*Math.PI/4) // Math.atan2 returns a number between -pi and pi
                return [swipe_origin, swipe_started, "LEFT" + hold_status];
            else if (rad > -3*Math.PI/4 && rad < -Math.PI/4)
                return [swipe_origin, swipe_started, "UP" + hold_status];
        }
        return [swipe_origin, swipe_started, "NONE"];
    },
    
    handle_touch: function(){
        var ex = this.pos.x; // entity position
        var ey = this.pos.y;
        ex = ex - ig.game.screen.x + this.size.x/2;
        ey = ey - ig.game.screen.y + this.size.y/2;
        var touch = [];
        var i = 0;
        var s = [];
        for (var t in ig.input.touches){
            var ans = this.swiped_where(this.swipe_origin[i], Boolean(this.swipe_started[i]), ig.input.touches[t]);
            this.swipe_origin[i] = ans[0];
            this.swipe_started[i] = ans[1];
            s[i] = ans[2];
            if (s[i]==this.swipes.up_released && this.is_standing())
                touch.push('jump');
            i++;
        }
        if (s.length > 0 && s[0] != 'NONE'){
            if (this.debug)
                console.log("swipe state = " + s[0]);   
        }

        for ( var t in ig.input.touches ){
            this.t = ig.input.touches[t];
                if (s ==this.swipes.up_released && this.is_standing())
                    touch.push('jump');            
                if ( this.t.x < ig.system.width/3 && this.t.y > ig.system.height/3)
                    touch.push('left');
                if( this.t.x > 2*ig.system.width/3 && this.t.y > ig.system.height/3)
                    touch.push('right');   
        }
        if (this.debug == 1)
            console.log('Egg = (' + ex + ',' + ey + ')\n Touch = ('+ this.t.x + ',' + this.t.y + ')\n Screen = (' + ig.game.screen.x + ',' + ig.game.screen.y+')');
        
        return touch;
    },
    
    handle_input : function(){
        this.entity_state = [];
        
        var touch_state = this.handle_touch();
        
        if ( ig.input.state("left") || touch_state.indexOf("left") >= 0 )
            this.entity_state.push("left");
        if ( ig.input.state("right") || touch_state.indexOf("right") >= 0)
            this.entity_state.push("right");
        if ( ig.input.state("jump") || touch_state.indexOf("jump") >= 0 )
            if (this.is_standing())
                this.entity_state.push("jump");
        if (this.entity_state.length == 0)
            this.entity_state = ["idle"];
        return this.entity_state;
    },
    
    update: function(x, y, settings){
        this.parent( x, y, settings );
        
        this.body.WakeUp();
        
        if (this.body.GetLinearVelocity().y > -1 && this.body.GetLinearVelocity().y < 1 && this.is_standing() == false )
            this.fall_y = this.body.GetPosition().y;
        
        if (this.is_standing()){
            if (-this.fall_y + this.body.GetPosition().y < 20){
                this.fall_y = this.body.GetPosition().y;
            }
            else{
                this.game_over = "game over";
            }
        }
        
        this.handle_input();
        if (this.game_over == "game over"){
            this.currentAnim = this.anims.broken;
            this.size.y = 13;
            if (this.game_over_time.delta() > 5)
                ig.game.loadLevel(LevelTest);
        } else {
            this.game_over_time.set(0);
            for (var i = 0; i < this.entity_state.length ; i++)
                switch (this.entity_state[i]){
                        case "left":
                            this.move_left();
                            break;
                        case "right":
                            this.move_right();
                            break;
                        case "jump":
                            this.jump();
                            break;
                        case "idle":
                            console.log("idle");
                            this.idle();
                            break;
                }
        }
        this.is_touching_wall();
        this.body.SetXForm(this.body.GetPosition(), 0);
        this.currentAnim.update();
        this.currentAnim.draw();
        
    }
});
});