ig.module(
	'game.entities.egg'
)
.requires(
    'plugins.box2d.entity'
)
.defines(function(){

EntityPlatform = ig.Entity.extend({
	debug : 0, // prints debug messages if it is 1
	// The players (collision) size is a bit smaller than the animation
	// frames, so we have to move the collision box a bit (offset)
	size: {x: 16, y:16},
	collides: ig.Entity.COLLIDES.PASSIVE,
	
	animSheet: new ig.AnimationSheet( 'media/platforms.png', 16, 16 ),	
		
    
    //local variables
    speed : 20,
    
	init: function( x, y, settings ) {  
        this.t = ig.input.mouse; // touch input
        this.entity_state = ["idle"];
		// Add the animations
		this.addAnim( 'idle', .2, [0]);
        this.addAnim( 'broken', 1, [1]);
        
		this.parent( x, y, settings );
        if ( this.body != null){
            this.fall_y = this.body.GetPosition().y;
            this.body.SetBullet(true);
        }
	},
    
    update: function(x, y, settings){
        this.parent( x, y, settings );
        
        this.body.WakeUp();
        
        this.currentAnim.update();
        this.currentAnim.draw();
        
    }
});
});