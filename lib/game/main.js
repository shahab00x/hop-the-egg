ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
    'game.entities.egg',
    'plugins.button',
    'plugins.box2d.game',
    'plugins.box2d.debug',
    'plugins.multitouch',
    'game.levels.level1'
)
.defines(function(){

MyGame = ig.Box2DGame.extend({
	gravity: 300,
	// Load a font
	font: new ig.Font( 'media/04b03.font.png' ),
	bgtune: new ig.Sound('media/mario.*', false),
	
    debugMultiTouch: false,
    touchColors: {},
    
	init: function() {
		// Initialize your game here; bind keys etc.
        ig.input.bind( ig.KEY.UP_ARROW, 'up');
        ig.input.bind( ig.KEY.DOWN_ARROW, 'down');
        ig.input.bind( ig.KEY.LEFT_ARROW, 'left');
        ig.input.bind( ig.KEY.RIGHT_ARROW, 'right');
        ig.input.bind( ig.KEY.SPACE, 'jump');
    
        ig.input.bind( ig.KEY.MOUSE1, 'touch' );
        ig.input.bind( ig.KEY.MOUSE2, 'touch2' );
        
        ig.input.initMouse();
        
        ig.BackgroundMap.chunkSize = 1024;
        // music
        ig.music.add( this.bgtune);
        ig.game.gravity = 1000;
        ig.music.volume = 0;
        //ig.music.play();
        
        
        // load level
        this.loadLevel( LevelLevel1);
//        ig.game.levels.
        ig.world.friction = 1;
        this.DebugDrawer = new ig.Box2DDebug(ig.world);
//        this.spawnEntity(Button, 20, 10, {
//            font: new ig.Font('media/04b03.font.png'),
//            textPos: {
//                x: 10,
//                y: 10
//            },
//            textAlign: ig.Font.ALIGN.CENTER,
//            size: {
//                x: 30,
//                y: 30
//            },
//            animSheet: new ig.AnimationSheet('media/sound_off.png', 16, 16),
//            
//            pressedDown: function() {
//                console.log('pressedDown');
//            },
//            pressed: function() {
//                console.log('pressed');
//                
//            },
//            pressedUp: function() {
//                console.log('pressedUp');
//                ig.music.volume = 1-ig.music.volume;
//            }
//        });
        
        
	},
	
	update: function() {
		// Update all entities and backgroundMaps
		this.parent();
		
		// Add your own, additional update code here
        
        // screen follows the player
		var player = this.getEntitiesByType( EntityEgg )[0];
		if( player ) {
//			this.screen.x = player.pos.x - ig.system.width/2;
			this.screen.y = player.pos.y - ig.system.height + 50 ;
        }
	},
	
	draw: function() {
		// Draw all entities and backgroundMaps
		this.parent();
		
		
		// Add your own drawing code here
		var x = ig.system.width/2,
			y = ig.system.height/2;
        
//        this.DebugDrawer.draw();
		
	},
	
	loadLevel: function(data) {
		this.parent(data);
		for( var i = 0; i < this.backgroundMaps.length; i++ ) {
			this.backgroundMaps[i].preRender = true;
		}
	},
});


// Start the Game with 60fps, a resolution of 320x240, scaled
// up by a factor of 2
    var factor = 4;
    if ( ig.ua.mobile )
        ig.main( '#canvas', MyGame, 60, 256, 457  , 1 /* ig.ua.pixelRatio*/ );
    else
        ig.main( '#canvas', MyGame, 60, 256, 457  , 2 );

});
